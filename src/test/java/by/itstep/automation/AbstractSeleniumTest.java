package by.itstep.automation;

import by.itstep.automation.util.DriverUtil;
import com.github.javafaker.Faker;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

public abstract class AbstractSeleniumTest {

    protected static final Faker FAKE = Faker.instance(Locale.US, ThreadLocalRandom.current());

    protected WebDriver driver;

    @BeforeMethod
    protected void setUp() {
        driver = DriverUtil.driver();
    }

    @AfterMethod
    protected void shutDown() {
        driver.close();
    }
}
