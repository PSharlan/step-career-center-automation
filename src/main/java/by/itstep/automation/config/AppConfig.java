package by.itstep.automation.config;

import static by.itstep.automation.util.PropertyLoader.loadProperty;

public class AppConfig {

    public static final String BASE_URL;
    public static final String BROWSER_NAME;
    public static final String BROWSER_VERSION;
    public static final String DRIVER_LOCATION;

    static {
        BASE_URL = loadProperty("site.url");
        BROWSER_NAME = loadProperty("browser.name");
        BROWSER_VERSION = loadProperty("browser.version");
        DRIVER_LOCATION = loadProperty("driver.location");
        System.setProperty("webdriver.chrome.driver", DRIVER_LOCATION);
    }

}
