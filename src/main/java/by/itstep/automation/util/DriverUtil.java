package by.itstep.automation.util;

import by.itstep.automation.config.AppConfig;
import org.openqa.selenium.WebDriver;

public class DriverUtil {

    public static WebDriver driver() {
        return WebDriverFactory.getDriver(AppConfig.BROWSER_NAME);
    }

}
