package by.itstep.automation.util;

import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {

    private static final String PROPERTIES_PATH = "/application.properties";

    private PropertyLoader() {
    }

    public static String loadProperty(final String name) {
        final Properties props = new Properties();
        try {
            props.load(PropertyLoader.class.getResourceAsStream(PROPERTIES_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return props.getProperty(name);
    }

}
