package by.itstep.automation.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import static java.lang.String.format;

public class WebDriverFactory {

    public static final String CHROME = "chrome";
    public static final String FIREFOX = "firefox";
    public static final String INTERNET_EXPLORER = "ie";

    public static WebDriver getDriver(String browserName) {
        if (CHROME.equalsIgnoreCase(browserName)) {
            return new ChromeDriver();
        } else if (FIREFOX.equalsIgnoreCase(browserName)) {
            return new FirefoxDriver();
        } else if (INTERNET_EXPLORER.equalsIgnoreCase(browserName)) {
            return new InternetExplorerDriver();
        }

        throw new IllegalArgumentException(format("Unsupported browser: %s", browserName));
    }
}
