package by.itstep.automation.page;

import by.itstep.automation.config.AppConfig;
import by.itstep.automation.page.shared.Menu;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage {

    private static final String URL = AppConfig.BASE_URL + "/";

    private Menu menu;
    private WebDriver driver;
    private WebDriverWait wait;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        menu = new Menu(driver);
    }

    public void open() {
        driver.get(URL);
    }

    public Menu getMenu() {
        return menu;
    }
}
